# Automated deployment on IBM Kubernetes Service (IKS)

This guide describes my IKS demo which was part of my Session at Social Connections 14 in Berlin (https://socialconnections.info/sessions/running-microservices-in-production-with-ibm/). I recommend reviewing all scripts this guide is using. 

## Create IBM Cloud Account
You will need an IBM Cloud account (https://console.bluemix.net/) which you will use to create a IBM Cloud Kubernetes Lite Cluster. This cluster is free of charge.

## Create Gitlab project
You will also need a Gitlab account (https://gitlab.com/) as well as a project (both is free of charge).

Create your own Gitlab project and clone the demo repo (https://gitlab.com/nmeisenzahl/soccnx14-demo) which contains all the needed files.

## Create IBM Cloud Kubernetes Cluster

### Setup CLIs for IKS

First of all, you need to create your free account here: https://console.bluemix.net/

Execute `iks/1-setup-cli.sh` which will install all needed tools and CLIs. This will include:
- ibmcloud cli
- ibmcloud plugins (container-service, container-registry)
- kubectl
- helm
- git
- docker cli

It can also be used to upgrade a previous installation (bx).
More details: https://console.bluemix.net/docs/cli/index.html#overview

### Create IBM Cloud Kubernetes Lite (Free) Cluster

Execute `iks/2-create-lite-cluster.sh` to deploy a new Lite Cluster. Customize the script based on your requirements.

It will take some minutes until the Cluster is ready.

### Configure your CLIs

Execute `iks/3-configure-cli.sh` to configure kubectl as well as Helm.

## Automated deployment with IBM Cloud

To build an automated deployment with IBM Cloud you can use the IBM Cloud Toolchain (https://console.bluemix.net/devops/create) as well as  IBM Cloud DevOps Insights (https://console.bluemix.net/catalog/services/devops-insights). In my session example I used the following toolchain:
- THINK: Git issue integration
- CODE: GitLab projects integration, Web CLI
- DELIVER: Build (Dockerfile), Deploy (Deployment to IKS CLuster)
- LEARN: DevOps Insight integration
- CULTURE: Slack channel integration

## Automated deployment with Gitlab

### Configure Gitlab IKS Runner

Execute `iks/gitlab-iks-init.sh` to configure IKS for Gitlab. Afterward, install the Kubernetes Runner and Helm using the Gitlab UI (https://gitlab.com/help/user/project/clusters/index).

### Build and deploy the "Hello World" go app

The app will be built as well as deployed using a Gitlab pipeline which is defined by `.gitlab-ci.yml`. The build and test stages will run on every commit. A commit to the master branch will also run the deploy stage. Update the pipeline based on your environment. The file needs to be located in the root directory of your project.

## Cleanup your environment

Your created Lite Cluster will be deleted after 30 days. In case you would like to delete it just execute `iks/delete-cluster.sh`. If you like, you could also drop your Gitlab project as well as delete your IBM Cloud account.